var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var http = require('http').Server(app)
var io = require('socket.io')(http)
var mongoose = require('mongoose')

var model = require("./model")

// var dbURL = 'mongodb://node:node@ds119446.mlab.com:19446/nodejs_learning_with_mongodb'


app.use(express.static(__dirname));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended : false}))

var Message = model.Message
 

app.get('/messages', (req, res)=>{
    Message.find({},(err, messages)=>{
        res.send(messages)
    })
})

app.post('/messages', (req, res)=>{
    var message = new Message(req.body)

    message.save((err)=>{
        if(err){ sendStatus(500)}
        Message.findOne({message: 'badword'}, (err,censored)=>{
            if (censored){
                Message.remove({_id:censored.id}, (err)=>{
                    console.log(censored, "bad word removed")
                })
            }
        })
        io.emit('message',req.body)
        res.sendStatus(200);
    })
})
io.on('connection',()=>{
    console.log('a user connected')
})


var server = http.listen(8000, ()=>{
    console.log("Server is listning at port ",server.address().port);
}); 