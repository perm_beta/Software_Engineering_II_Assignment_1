********************************
*  Eyoel Wendwosen             *
*  ATR/7467/08                 *
*  Section 2                   *
********************************

This project is a simple MVC application, developed for learning purpose. 
It's a simmple Chat application that allows users to exchange messages. 

The server runs on port 8000 at localhost, after that it connects to MongoDB database client.
Then any user browsing on http://localhost:8000 will connect to the server and chat with other people

The MVC pattern is structured on these files.

Model - model.js
View - script.js / index.html
Controller - server.js