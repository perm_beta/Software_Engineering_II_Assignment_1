var io = io()
$(() => {
    $("#send").click(()=>{
        var message = {
            name : $('#name').val(),
            message : $('#message').val()
        }
        postMessage(message)
    })
    // while(true) 
    getMessages()
    
})

io.on('message', addMessage)

function addMessage(message){
    $("#messages").append(`<h4> ${message.name} </h4> <p> ${message.message} </p>`)
}

function getMessages(){
    $.get('http://localhost:8000/messages',(data) =>{
       data.forEach(addMessage);
    })
}

function postMessage(message) {
        $.post('http://localhost:8000/messages', message)
}
