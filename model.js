var mongoose = require('mongoose')

var dbURL1 = "mongodb://localhost:27017"

exports.Message = mongoose.model('Message',{
    name : String,
    message : String
})

mongoose.connect(dbURL1, {useMongoClient : true}, (error)=>{
    console.log('MongoDb database connection: ',error)
})
